//
//  Petitions.swift
//  Project7
//
//  Created by Danni Brito on 11/9/19.
//  Copyright © 2019 Danni Brito. All rights reserved.
//

import Foundation

struct Petitions: Codable{
    var results: [Petition]
}
